-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-09-2022 a las 23:20:29
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `eloquent`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `destinations`
--

CREATE TABLE `destinations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `destinations`
--

INSERT INTO `destinations` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Lima', '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(2, 'Arequipa', '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(3, 'Cusco', '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(4, 'Trujillo', '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(5, 'Ica', '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(6, 'Piura', '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(7, 'Chiclayo', '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(8, 'Huancayo', '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(9, 'Tacna', '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(10, 'Pucallpa', '2022-09-05 05:55:05', '2022-09-05 05:55:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `flights`
--

CREATE TABLE `flights` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `legs` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `departed` tinyint(1) NOT NULL DEFAULT 0,
  `arrived_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `destination_id` bigint(20) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `flights`
--

INSERT INTO `flights` (`id`, `name`, `number`, `legs`, `active`, `departed`, `arrived_at`, `destination_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Winona Smith', '70383', 2, 1, 1, '1976-08-08 04:36:20', 2, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(2, 'Carlotta Wilderman', '67200', 4, 0, 1, '2021-01-06 10:15:35', 7, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(3, 'Newton Volkman', '83270', 4, 1, 1, '2017-12-05 12:23:31', 1, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(4, 'Dedrick Bernier', '68414', 3, 0, 1, '1976-12-02 23:27:33', 9, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(5, 'Myrtie Connelly', '34203', 3, 1, 0, '1975-12-13 20:34:25', 9, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(6, 'Teagan Macejkovic', '93608', 4, 0, 0, '2012-03-26 02:01:01', 10, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(7, 'Armani Auer', '43857', 3, 0, 0, '1991-09-02 06:42:20', 1, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(8, 'Mrs. Trisha Considine DDS', '724', 2, 0, 0, '1995-01-20 05:25:04', 1, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(9, 'Gisselle Christiansen', '9027', 3, 0, 0, '1972-12-03 16:05:13', 9, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(10, 'Leatha Kautzer', '4624', 1, 0, 1, '2013-07-27 05:54:49', 1, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(11, 'Ola Christiansen Sr.', '8363', 2, 1, 0, '2022-05-09 01:00:46', 3, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(12, 'Elton Lakin', '85272', 5, 1, 1, '2015-02-24 12:26:24', 10, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(13, 'Barton Yost', '77224', 4, 1, 1, '1994-05-15 10:08:07', 6, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(14, 'Johann Ruecker', '27594', 4, 0, 0, '1987-06-01 00:31:20', 10, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(15, 'Darwin Barrows', '45238', 5, 1, 1, '2018-07-01 21:02:42', 7, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(16, 'Tessie Denesik', '8371', 5, 0, 0, '1970-04-18 06:19:38', 7, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(17, 'Dr. Odessa Bergnaum MD', '78268', 1, 0, 1, '2012-06-06 06:46:10', 7, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(18, 'Darby Jacobi II', '81333', 1, 1, 1, '2012-12-23 02:53:07', 6, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(19, 'Rodrick Cummings', '82942', 3, 0, 0, '2012-08-27 05:59:59', 4, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(20, 'Romaine Kuhn', '31888', 4, 1, 0, '1979-10-30 15:57:13', 4, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(21, 'Mr. Johnathon Bergstrom', '18215', 2, 1, 1, '2009-10-21 22:48:01', 4, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(22, 'Ali Steuber III', '7760', 3, 1, 1, '2001-06-10 12:23:04', 9, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(23, 'Dr. Alana Powlowski', '23040', 2, 1, 1, '2009-11-08 12:18:41', 10, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(24, 'Mrs. Nelda Bogisich', '85858', 1, 1, 0, '1971-02-07 07:17:54', 4, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(25, 'Colby Hane IV', '80923', 2, 1, 0, '2004-10-03 04:17:01', 2, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(26, 'Sibyl West', '13850', 5, 0, 0, '1990-02-27 17:43:37', 3, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(27, 'Dolly Aufderhar', '20927', 1, 1, 1, '1998-10-04 01:46:41', 9, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(28, 'Mr. Bradly Donnelly', '69292', 4, 0, 0, '2000-05-18 10:57:02', 9, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(29, 'Molly Langosh', '45817', 1, 0, 1, '2010-04-16 18:36:56', 6, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(30, 'Prof. Cleveland Fritsch', '96393', 4, 1, 1, '2017-01-23 10:35:47', 10, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(31, 'Barney Franecki', '74752', 3, 0, 0, '2000-01-02 18:04:06', 6, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(32, 'Kristy Cronin', '11692', 5, 1, 0, '2019-12-27 02:36:17', 3, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(33, 'Mr. Marlin Cronin DDS', '45036', 1, 1, 1, '1998-07-14 03:42:26', 10, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(34, 'Mrs. Jodie Schowalter', '93072', 4, 1, 1, '2004-09-04 13:59:18', 2, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(35, 'Prof. Javonte Maggio DVM', '2101', 2, 1, 1, '1986-08-06 02:42:47', 7, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(36, 'Schuyler Rippin', '27826', 3, 0, 1, '2000-03-25 07:24:38', 4, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(37, 'Tyler Mayert', '79626', 2, 0, 0, '2017-03-05 12:26:48', 4, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(38, 'Beryl Denesik', '40499', 4, 0, 1, '2008-01-17 08:06:57', 7, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(39, 'Prof. Holly Anderson', '66314', 3, 0, 1, '1998-06-28 01:41:24', 10, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(40, 'Heaven Toy', '62349', 3, 1, 1, '2018-05-27 21:38:04', 8, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(41, 'Bulah Miller', '10073', 3, 0, 0, '2008-09-15 00:56:07', 4, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(42, 'Johnny Emmerich', '84818', 3, 1, 0, '2017-12-28 09:49:24', 4, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(43, 'Murray Gerhold', '99119', 1, 1, 1, '2000-09-12 02:06:23', 10, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(44, 'Lydia Murazik', '68478', 2, 0, 1, '1976-11-13 18:23:37', 2, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(45, 'Morgan O\'Connell', '53217', 4, 0, 1, '2019-08-27 18:32:44', 2, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(46, 'Friedrich Hilpert', '9757', 4, 1, 1, '2001-02-15 03:27:44', 6, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(47, 'Prof. Jarvis Bernier DVM', '56738', 3, 1, 1, '1979-01-14 16:06:38', 10, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(48, 'Selina Powlowski Sr.', '52598', 3, 0, 1, '2010-04-23 22:27:05', 8, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(49, 'Shanon Ziemann', '31255', 5, 0, 0, '2014-05-29 05:58:27', 9, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(50, 'Dr. Sally Cole', '41076', 3, 1, 1, '1978-10-27 11:37:00', 8, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(51, 'Eileen Davis', '82858', 2, 0, 1, '1989-04-09 04:09:37', 4, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(52, 'Prof. Adolphus Lemke', '2922', 5, 1, 0, '2010-03-21 19:40:01', 6, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(53, 'Francis Gutkowski', '5013', 2, 0, 0, '2015-12-26 16:48:10', 4, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(54, 'Tressie Murray V', '1165', 2, 0, 0, '1974-04-06 04:01:49', 4, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(55, 'Jalyn Kub PhD', '81638', 1, 1, 1, '1992-11-26 03:12:34', 4, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(56, 'Mortimer Heidenreich', '24505', 2, 1, 0, '1989-10-03 00:12:07', 6, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(57, 'Santa Harber Jr.', '51306', 2, 1, 1, '2001-08-24 22:55:24', 8, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(58, 'Imelda Rolfson I', '7536', 3, 0, 0, '1994-11-03 06:57:10', 4, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(59, 'Ruby Baumbach', '14037', 1, 0, 1, '2007-12-19 18:52:07', 3, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(60, 'Terrence Treutel', '45065', 5, 1, 0, '1990-09-18 07:54:45', 5, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(61, 'Josie Dickinson', '40214', 5, 0, 1, '1992-06-01 14:10:50', 3, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(62, 'Jolie Heathcote', '89836', 5, 0, 1, '2011-05-16 15:12:31', 1, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(63, 'Hannah Heidenreich', '58768', 2, 1, 0, '2005-04-10 01:30:07', 7, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(64, 'Dr. Rosalee Roberts Sr.', '60282', 1, 0, 0, '1979-09-23 01:26:48', 6, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(65, 'Wilhelm Oberbrunner', '29225', 5, 0, 1, '1985-02-15 14:17:43', 7, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(66, 'Dasia Kuhlman MD', '78279', 2, 1, 1, '2021-04-25 16:20:02', 7, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(67, 'Mrs. Edyth Batz PhD', '59119', 2, 1, 0, '1987-01-22 21:27:52', 2, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(68, 'Miss Rafaela Goodwin', '66089', 5, 1, 0, '2008-04-21 01:30:54', 4, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(69, 'Mayra Runte', '84740', 1, 1, 0, '2003-04-06 20:22:28', 1, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(70, 'Ms. Nikita Cormier IV', '15623', 4, 0, 1, '1971-04-19 00:15:10', 8, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(71, 'Emmy Oberbrunner', '61912', 2, 1, 1, '1983-04-04 15:52:41', 2, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(72, 'Jalyn Corkery', '10010', 4, 0, 1, '2016-12-03 07:14:10', 4, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(73, 'Ray Lang', '96881', 5, 0, 0, '1998-01-14 00:23:24', 2, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(74, 'Levi Gibson', '39917', 1, 1, 1, '2002-05-18 12:15:25', 7, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(75, 'Orpha Satterfield', '41565', 4, 1, 1, '1977-04-13 07:18:03', 4, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(76, 'Mr. Raleigh Gusikowski', '40794', 3, 0, 0, '2012-01-08 11:29:32', 3, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(77, 'Nellie Heathcote V', '75986', 1, 1, 0, '2021-10-30 22:01:41', 10, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(78, 'Prof. Arielle Keeling', '33227', 1, 0, 1, '2003-04-10 06:44:17', 9, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(79, 'Yazmin Cummings', '79138', 2, 1, 1, '1983-06-27 11:33:47', 9, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(80, 'Prof. Ophelia Spencer', '19012', 3, 0, 1, '2006-07-18 02:15:50', 1, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(81, 'Al Will', '30916', 4, 0, 0, '2003-11-28 14:15:04', 2, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(82, 'Kris Quitzon', '43689', 2, 0, 0, '2019-10-29 13:20:21', 4, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(83, 'Bill Sawayn Sr.', '59834', 3, 0, 0, '1979-09-16 20:54:30', 7, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(84, 'Ardella Osinski PhD', '77288', 5, 0, 1, '2008-05-21 01:57:09', 10, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(85, 'Dayana Jast', '5522', 5, 1, 1, '1978-07-29 07:44:30', 2, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(86, 'Hellen Braun', '93597', 3, 0, 1, '1971-09-13 02:43:07', 4, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(87, 'Octavia Pfannerstill', '93278', 5, 0, 0, '1977-09-27 06:39:23', 6, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(88, 'Clifton Casper', '70474', 2, 1, 1, '2012-10-18 18:08:02', 4, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(89, 'Prof. Leta Kovacek', '76445', 2, 0, 1, '1992-05-18 22:01:13', 2, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(90, 'Isom Larson', '98790', 1, 0, 1, '1972-05-25 06:16:50', 9, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(91, 'Gertrude Bergstrom', '79379', 2, 0, 1, '2006-07-15 02:47:54', 10, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(92, 'Lexus Murazik PhD', '86763', 5, 1, 0, '1976-02-03 07:23:31', 8, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(93, 'Jacklyn Becker', '47003', 2, 0, 1, '2018-02-23 18:33:24', 10, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(94, 'Eusebio Mayer IV', '1343', 1, 0, 0, '1999-01-26 16:55:16', 1, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(95, 'Rupert Bergnaum', '28100', 4, 1, 0, '1990-05-29 01:47:13', 1, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(96, 'Clementine Rogahn', '81619', 5, 1, 0, '1978-12-31 11:03:17', 7, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(97, 'Mable Moen', '72248', 5, 0, 0, '2018-12-06 01:42:01', 6, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(98, 'Bonnie Zemlak', '68574', 1, 1, 1, '2009-01-30 03:01:42', 1, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(99, 'Prof. Precious Price', '74611', 4, 0, 1, '1970-07-31 12:57:11', 6, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(100, 'Breana Gerhold', '61353', 1, 1, 0, '1982-07-01 10:53:46', 3, NULL, '2022-09-05 05:55:05', '2022-09-05 05:55:05'),
(101, 'Victor Arana Flores', '123', 4, 0, 1, '2022-09-05 03:58:25', 7, NULL, '2022-09-05 08:58:25', '2022-09-05 08:58:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_08_27_015104_create_destinations_table', 1),
(6, '2022_08_27_015733_create_flights_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `destinations`
--
ALTER TABLE `destinations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indices de la tabla `flights`
--
ALTER TABLE `flights`
  ADD PRIMARY KEY (`id`),
  ADD KEY `flights_destination_id_foreign` (`destination_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `destinations`
--
ALTER TABLE `destinations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `flights`
--
ALTER TABLE `flights`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `flights`
--
ALTER TABLE `flights`
  ADD CONSTRAINT `flights_destination_id_foreign` FOREIGN KEY (`destination_id`) REFERENCES `destinations` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
