<?php

use App\Models\Post;
use App\Models\User;
use App\Models\Flight;
use App\Models\Destination;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});
Route::get('/prueba', function () {
    // $flights = Flight::where('active', 1)->orderBy('name', 'desc')->take(5)->get(); // where
    // Modificar masivamente millones de datos en BD par no quedarnos sin memoria
    // foreach (Flight::cursor() as $flight) {
    //     $flight->active = true;
    //     $flight->save();
    // }
    // return Flight::all();
    /**************************
     * Subconsultas avanzadas cruzadas*
     **************************/
    $destinations = Destination::addSelect([
        'last_flight' => Flight::select('number')
            ->whereColumn('destination_id', 'destinations.id')
            ->orderBy('arrived_at', 'desc')
            ->limit(1)])->get();
    return $destinations;
});
/****************************************************
 * Recuperación de modelos individuales en Eloquent *
 ****************************************************/
Route::get('recuperando-modelos-individuales', function () {
    // $flight = Flight::find(6); //consultar por ID]
    // $flight = Flight::where('departed', true)->get(); //consultar por condición
    // $flight = Flight::where('departed', true)->first(); //consultar el primer registro que cumpla la condición - Opción #1
    // $flight = Flight::firstWhere('departed', true); //consultar el primer registro que cumpla la condición - Opción #2
    /************************************************
     * Para consultas en caso no exista el registro *
     ************************************************/
    // $flight = Flight::findOr(201, function () {
    //     return 'No existe el vuelo';
    // });
    // $flight = Flight::where('legs', '>', 6)->firstOr(function () {
    //     return 'No existe el vuelo';
    // });
    /************************
     * Utilizando Exception *
     ************************/
    // $flight = Flight::findOrFail(201);
    // $flight = Flight::where('legs', '>', 6)->firstOrFail();
    // return $flight;

    /***************************************
     * Si no existe el registro lo creamos *
     ***************************************/
    // $destination = Destination::firstOrCreate([
    //     'name' => 'Puno',
    // ]);
    // return $destination;
    // $flight = Flight::firstOrCreate([
    //     'name' => 'Kevyncito H', //verifica que exista el registro, si no existe creamos con el segundo array incluyendo el campo verificado
    // ], [
    //     'number' => '123456',
    //     'legs' => 2,
    //     'active' => true,
    //     'departed' => false,
    //     'arrived_at' => now(),
    //     'destination_id' => 1,
    // ]);
    // $flight = Flight::firstOrNew([
    //     'name' => 'MKevyn HHH', //verifica que exista el registro, si no existe creamos con el segundo array incluyendo el campo verificado
    // ], [
    //     'number' => '123456',
    //     'legs' => 2,
    //     'active' => true,
    //     'departed' => false,
    //     'arrived_at' => now(),
    //     'destination_id' => 1,
    // ]);
    // $flight->save(); // se agrega para utilizar con firstOrNew para guardar en BD indispensablemente
    // Otros ejemplos
    // $flight = Flight::where('departed', true)->count(); //contar
    // $flight = Flight::where('departed', true)->sum('legs'); // sumar
    // $flight = Flight::where('departed', true)->max('legs'); // máximo
    // $flight = Flight::where('departed', true)->avg('legs'); // promedio
    // return $flight;
});
/****************************************************
 * Inserción y actualización de modelos en Eloquent *
 ****************************************************/
Route::get('insercion-actualizacion', function () {
    //* Para crear
    // $destination = new Destination();
    // $destination->name = 'Huánuco';
    // $destination->save(); //indispensable para guardar en BD
    //* Para actualizar
    // $destination = Destination::find(12);
    // $destination->name = 'Ancash';
    // $destination->save();
    // return $destination;
    //*guardar
    // $data = [
    //     'name' => 'MKevyn HH',
    //     'number' => '123',
    //     'legs' => 1,
    //     'active' => true,
    //     'departed' => false,
    //     'arrived_at' => now(),
    //     'destination_id' => 1,
    // ];
    //? CREAR - Para esta opción de registro no es necesariod definir la asignación masica de $fillable en el modelo
    // $flightCreate = new Flight();
    // $flightCreate->name = $data['name'];
    // $flightCreate->number = $data['number'];
    // $flightCreate->legs = $data['legs'];
    // $flightCreate->active = $data['active'];
    // $flightCreate->departed = $data['departed'];
    // $flightCreate->arrived_at = $data['arrived_at'];
    // $flightCreate->destination_id = $data['destination_id'];
    // $flightCreate->save();
    //? CREAR - Para esta opción create y update es indispensable definir la asignación masica en el modelo
    // $flight = Flight::create($data);
    //* ACTULIZAR masivamente
    // $flight = Flight::find(110);
    // $flight->update($data);
    // return $flight;
    //* Actualizar o crear si no existe el registro
    // $flight = Flight::updateOrCreate([
    //     'name' => 'MKevyn HH',
    // ], [
    //     'number' => '123',
    //     'legs' => 1,
    //     'active' => true,
    //     'departed' => false,
    //     'arrived_at' => now(),
    //     'destination_id' => 1,
    // ]);
    // return $flight;
});

/**************************************
 * Eliminación de modelos en Eloquent *
 **************************************/
Route::get('eliminacion-modelo', function () {
    //* Eliminación forma #1
    // $flight = Flight::find(112);
    // $flight->delete();
    //* Eliminación forma #2
    // Flight::destroy(97);
    // Flight::destroy([99, 98, 97, 96]); //Eliminar varios registros
    // Flight::truncate(); //Eliminar todos los datos de la tabla

    //* Eliminación con condiciones
    // Flight::where('active', 0)->delete();
    //* Eliminación trabajando con softDeletes
    // Flight::destroy(100);
    // return Flight::orderBy('id', 'desc')->get(); //Ver todos los registros
    // return Flight::orderBy('id', 'desc')->withTrashed()->get(); // Ver registros eliminados con softDeletes
    // return Flight::orderBy('id', 'desc')->onlyTrashed()->get(); // Ver registros sólo eliminados  softDeletes
    // Flight::where('id', 98)->withTrashed()->restore(); // Restaurar registro eliminado
    // Flight::where('id', 97)->withTrashed()->forceDelete(); // Eliminar registro definitivamente
    // return Flight::orderBy('id', 'desc')->onlyTrashed()->get();
    //* Verificar que el registro se encuentra en papelera de reciclaje
    // $flight = Flight::where('id', 100)->withTrashed()->first();
    // if ($flight->trashed()) {
    //     return "El registro se encuentra en la papelera de reciclaje";
    // }
    // return "El registro no se encuentra en la papelera de reciclaje";
});
/****************************
 * Query Scopes en Eloquent *
 ****************************/
Route::get('query-scope', function () {
    /*********************************
     * TRABAJANDO CON SCOPE GLOBALES *
     *********************************/
    //* Considera el Scope GLOBAL
    // return Flight::all();
    //* Cuando no queremos que se condidera el scope GLOBAL - elimina o ignora el scope que le pasamos en ese consulta
    // return Flight::withoutGlobalScope(NotDepartedScope::class)->get();
    //si queremos varios scopes - si no usamos scopes aparte, se le pasa el nombre del método de scope del modelo
    // return Flight::withoutGlobalScopes(['not_departed'])->get();
    // return Flight::withoutGlobalScopes([NotDepartedScope::class])->get();
    /********************************
     * TRABAJANDO CON SCOPE LOCALES *
     ********************************/
    //*vereficar los activos
    // return Flight::active()->get();
    //*pasarle información al scope
    // return Flight::legs(4)->get();
});
/**************************************
 * TRABAJANDO CON EVENTOS EN ELOQUENT *
 **************************************/
Route::get('eventos-eloquent', function () {

    // Flight::create([
    //     'name' => 'MKevyn HH',
    //     // 'number' => '123' // eso lo pasamos desde el observer
    //     'legs' => 4,
    //     'active' => 0,
    //     'departed' => 1,
    //     'destination_id' => 7,
    //     'delete_at' => null,
    // ]);
    // return Flight::all();
});

/**************************************
/*****************************
 * TRABAJANDO CON RELACIONES *
 *****************************/
Route::get('relaciones-eloquent', function () {
    //* Recuperando el teléfono del usuario
    // $user = User::find(1);
    // return $user->phone;
    //* Recuperando el usuario por número de teléfono
    // $phone = Phone::find(1);
    // return $phone->user;
    // * Recuperando el usuario y sus cursos
    // $user = User::find(7);
    // return $user->courses;
    // * Recuperando el curso y el usuario que lo creó
    // $course = Course::find(9);
    // return $course->user;
    // * Recuperando último curso creado por el usuario
    // $user = User::find(1);
    // return $user->latestCourses;
    // return $user->oldestCourses;
    // * Recuperando el dueño del carro mediante el mecánico
    // $mechanic = Mechanic::find(1);
    // // return $mechanic->car; //saber el carro
    // return $mechanic->owner; //consultar el dueño del carro
    // * Recuperando la lección del CURSO a traves de la SECCIÓN
    // $course = Course::first();
    // return $course; // retorna el curso
    // return $course->sections; // retorna la secciones del curso
    // return $course->lessons; // retorna la lecciones del curso
});
/******************************************
 * Relaciones muchos a muchos en Eloquent *
 ******************************************/
Route::get('relacion-muchos-a-muchos', function () {
    //* Consultar Roles por el usuario
    // $user = User::find(1);
    // return $user->roles; //consulta los roles del Usuario
    // return $user->roles()->wherePivot('active', true)->get(); //consulta los roles agregamos condiciones para el campo active
    //*Consultar usuarios por el rol
    // $role = Role::first();
    // return $role->users;
});
/***************************************
 * Relaciones polimórficas en Eloquent *
 ***************************************/
Route::get('relacion-polimorfica', function () {
    //* curso y su imagen
    // $course = Course::find(1);
    // return $course->image->url;
    //* post y su imagen
    // $post = Post::find(1);
    // return $post->image;
    //* post y su comentario
    // $post = Post::find(1);
    // return $post->comments;
    //* curso y su comentario
    // $course = Course::find(1);
    // return $course->comments; //Retornar todos los comentarios
    // return $course->latestComment; // retornar el último comentario del curso
    // return $course->oldestComment; // retornar el último comentario del curso
    // return $course->tags; // retornar cursos y sus etiquetas
});
Route::get('consultas-relaciones', function () {
    // $user = User::find(1);
    // return $user->posts; //Para ver todos los posts del usuario
    // return $user->posts()->where('active', true)->get(); //Para pasarle filtros
    // return $user->posts()->where(function ($query) {
    //     $query->where('active', true)->orWhere('likes', '>=', 500);
    // })->get(); //Para pasarle filtros y orWhere agrupando para dar prioridad al active con likes y luego recién traer el usuario con id seleccionado
    //* Verificamos que el usuario no tenga un registro asociado a traves de la relacion posts y adicional que tengas mayor a  4 posts
    // $users = User::has('posts', '>', 4)->get();
    // return $users;
    //* Verificamos que el curso tenga una sección asociado y que tenga una lección
    // $courses = Course::has('sections.lessons')->get(); //cuando no tienes una relación a través de otro modelo
    // $courses = Course::has('lessons')->get(); // utilizando la relación a trav´es de otro modelo
    // return $courses;
    //*Para ver que usuarios hayan escrito palabras o temas iguales o similares dentro de la tabla posts con relación posts y el campo title
    // $users = User::whereHas('posts', function ($query) {
    //     $query->where('title', 'like', '%Expedita%');
    // })->get();
    //* Para usuarios que no han escrito un posts
    // $users = User::doesntHave('posts')->get();
    // return $users;
    //* Para consultar los comentarios de los cursos de la tabal polimorfica - recibe 3 parámetros la relación el modelo y function anónima para pasar los filtros
    // $comments = Comment::whereHasMorph('commentable', Post::class, function ($query) {
    //     return;
    // })->get();
    // return $comments;
});
/*****************************************************************
 * CARGA ANSIOSA EN ELOQUENT: ACELERANDO TUS APLICACIONE LARAVEL *
 *****************************************************************/
Route::get('carga-ansiosa-eloquent', [PostController::class, 'index']);

/*****************************************************************
 * Inserción y actualización de modelos relacionados en Eloquent *
 *****************************************************************/
Route::get('insertar-actualizar-modelos-relacionados', function () {
    //* Para insertar instanciando la clase
    // $comment = new Comment();
    // $comment->body = 'Comentario de prueba';
    // $course = Course::find(1);
    // $course->comments()->save($comment);
    //* Insertar masivamente no funciona éste tipo para crear o registrar relación de muchos a muchos.
    // $course->comments()->create([
    //     'body' => 'Comentario de prueba por asignación masiva',
    // ]);
    // return 'Comentario registrado correctamente';
    //* Para registrar campos con relación muchos a muchos
    // $user = User::find(1);
    // $user->roles()->attach(1); // registrar un solo rol
    // $user->roles()->attach([1, 2, 3]); // registrar varios roles pero si vuelves a ejecutar se va duplicar los registros
    // $user->roles()->detach([1, 2]); // registrar quitar roles
    // $user->roles()->sync([1, 3]); // Sincroniza los roles si no existe lo agrega caso contrario lo quita
    //* Recomendado registro con relaciones de muchos a muchos
    // $user->roles()->sync([
    //     1 => ['active' => false],
    //     3 => ['active' => true],
    // ]); // Sincroniza los roles si no existe lo agrega caso contrario lo quita, tambien agregar datos los demas campos con es el caso del active
    //* También con attach pero no es el recomendado porque duplica si se ejecuta varias veces el mismo rol
    // $user->roles()->attach([
    //     1 => [
    //         'active' => 1,
    //     ],
    // ]);
    // return 'Rol asignado correctamente';
});
/******************************************
 * Trabajando con colecciones en Eloquent *
 ******************************************/
Route::get('colecciones-eloquent', function () {
    //* Opción para verificar que cierto usuario exista dentro de la colección
    // $users = User::all();
    // $user = User::find(1);
    // if ($users->contains($user)) {
    //     return "El usuario existe dentro de la colección";
    // }
    // return 'El usuario no existe en la colección';
    //* Usuarios cuyo ID sea lo siguiente
    // $users2 = User::whereIn('id', [2, 3,4])->get();
    // $users2 = User::whereIn('id', [1, 2, 3])->get();
    //recuperamos todos los usuario de la primera colección pero que no se encuentran dentro de la segunda colección es decir que excluya los usuarios de las segunda colección
    // return $users->diff($users2); //opción #1
    // return $users->except([1,2,3]); //opción #2
    //*Recuperar información de la intersección de 2 colecciones
    // return $users->intersect($users2);
    //* Recuperando cierto registro de una colección
    // return $users->find(1);
    //* trabajando con carga anciosa
    // $users = User::get();
    //*carga anciosa a partir de una colección
    // return $users->load('posts');
    //*Recuperando solo el id de los usuarios
    // return $users->modelKeys();
    //*Para mostrar los campos ucultos
    // return $users->makeVisible(['password']);
    //*Para ocultar los campos visibles
    // return $users->makeHidden(['email']);
    //*Para recuperar solo los campos con id siguientes
    // return $users->only([1,2,3]);
    //*Para recuperar solo los usuarios únicos
    // return $users->unique();
});
/***************************************************************
 * Atributo Casting en Eloquent: Convertir y castear tus datos *
 ***************************************************************/
Route::get('atributo-casting-convertir-y-castear-datos', function () {
    //* creamos un registro y agregamos un campo json_encode
    // $meta = [
    //     'description' => 'es la descripción del curso',
    //     'keywords' => 'curso, laravel. php'
    // ];
    // $post = Post::create([
    //     'meta' => json_encode($meta),
    //     'title' => 'titulo de post22',
    //     'body' => 'contenido del post22',
    //     'user_id' => 1
    // ]);
    // return $post;
    //*consultamos el registro sin un caster
    // $post = Post::find(100);
    // $meta = json_decode($post->meta);
    // return $meta->description;
    //* creamos un registro con casts
    // $meta = [
    //     'description' => 'es la descripción del curso',
    //     'keywords' => 'curso, laravel. php'
    // ];
    // $post = Post::create([
    //     'meta' => $meta,
    //     'title' => 'titulo de post22',
    //     'body' => 'contenido del post22',
    //     'user_id' => 1
    // ]);
    // return $post;
        //*consultamos el registro sin un casts
    // $post = Post::find(102);
    // $meta = $post->meta; //puedes usar por separado o junto todo
    // return $post;
    // return $post->meta['description'];
});
/******************************************************
 * Serialización de modelos y colecciones en Eloquent *
 ******************************************************/
Route::get('serializacion-modelos-colecciones', function () {
    // $post = Post::get();
    // $post = Post::get()->makeVisible('id'); //podemos adicional mostrar el campo para que se visible
    // $post = Post::get()->makeHidden('meta'); //podemos adicional mostrar el campo para que se visible
    //* transformando colección a array
    // return $post->toArray()->first();//para verificar que es array
    //* transformando colección a json
    // return $post->toJson(); //opcion #1
    // return (string) $post; //opcion #1
    // return $post;
    //* Otros mutadares
    // $post = Post::find(1);
    // return $post->isAdmin;
    // $posts = Post::get();
    $posts = Post::get()->append('is_admin'); //para agregar un mutador si no quieres de modelo lo pasar aquí
    return $posts;
});
