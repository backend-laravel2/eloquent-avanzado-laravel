<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /****************************************************
     * PARA CAMBIAR EL DATA POR UN NOMBRE PERSONALIZADO *
     ****************************************************/
    public static $wrap = 'post';
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'body' => $this->body,
            'active' => $this->active,
            'user_id' => $this->user_id,
            'prueba' => $this->when(true,'prueba'),
            // 'user'=>$this->user, //relación user
            // 'user'=>new UserResource($this->user), //utilizando otro resource
            'user'=>new UserResource($this->whenLoaded('user')), //utilizando otro resource pero precargado es decir se va mostrar si nosotros hemos pedido que se cargue la relación user caso contrario no va cargar

        ];
    }
}
