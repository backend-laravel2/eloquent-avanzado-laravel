<?php

namespace App\Observers;

use App\Models\Flight;

class FlightObserver
{
    // Evento al momento de obtener algun registro
    public function retrieved(Flight $flight)
    {
        //podemos adicionar una propiedad al resultado
        $flight->prueba = 'prueba';
    }
    // Evento antes de crear
    public function creating(Flight $flight)
    {
        $flight->number = '123';
    }
}
