<?php

namespace App\Models;

use App\Models\Course;
use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;
    /************************************************
     * Relación muchos a muchos polimórfica inversa *
     ************************************************/
    public function courses()
    {
        return $this->morphedByMany(Course::class, 'tagglable');
    }
    public function posts()
    {
        return $this->morphedByMany(Post::class, 'tagglable');
    }
}
