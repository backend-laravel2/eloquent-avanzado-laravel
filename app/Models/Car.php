<?php

namespace App\Models;

use App\Models\Mechanic;
use App\Models\Owner;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;
    /*************************
     * Relación de uno a uno *
     *************************/
    public function owner()
    {
        return $this->hasOne(Owner::class);
    }
    /******************************
     * Relación inversa uno a uno *
     ******************************/
    public function mechanic()
    {
        return $this->belongsTo(Mechanic::class);
    }
}
