<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Destination extends Model
{
    use HasFactory;
    protected $fillable = ['name'];
    /*****************************************************************
     * Para utilizar otro campo de creado y actuliazado especificamos *
     *****************************************************************/
    // const CREATED_AT = 'creation_date';
    // const UPDATED_AT = 'updated_date';
    /********************************************************************
     * Para que un modelo específico se conecta a otro tipo de conexión *
     ********************************************************************/
    // protected $connection = 'sqlite';
    /**************************************************************
     * Para indicar que nuestra tabla se llama distinto al modelo *
     **************************************************************/
    // protected $table = 'list_destinations';
    /********************************************************************
     * Para indicar que nuestro campo identificador sea otro y no el ID *
     ********************************************************************/
    // protected $primaryKey = 'identificador';
    /**********************************************
     * Para que el campo no sea autoincrementable *
     **********************************************/
    // protected $incrementing = false;
    /**********************************************************************
     * Para indicar que el campo de llave primaria sea un campo de string *
     **********************************************************************/
    // protected $keyType = 'string';
    /*************************************************************************
     * Para indicar que no vamos utilizar el timestamps en nuestra migración *
     *************************************************************************/
    // public $timestamps = false;
    /*********************************
     * Para darle formato a la fecha *
     *********************************/
    // protected $dateFormat = 'd-m-Y';
    /*****************************************************************************
     * Mostrar valores por defecto si en caso no haya registros en nuestra tabla *
     *****************************************************************************/
    protected $attributes = [
        'name' => 'Lima',
    ];
}
