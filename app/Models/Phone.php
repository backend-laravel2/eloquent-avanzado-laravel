<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    use HasFactory;
    /*******************************************
     * TRABAJANDO CON RELACIONES ENTRE MODELOS *
     *******************************************/
    //* RELACIÓN UNO A UNO INVERSA
    // Inversa porque vamos a buscar con la llave foránea(user_id) y buscar la similitud con llave primaria (id) de la otra tabla
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
