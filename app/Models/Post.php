<?php

namespace App\Models;

use App\Casts\Meta;
use App\Models\Tag;
use App\Models\User;
use App\Models\Image;
use App\Models\Comment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Post extends Model
{
    use HasFactory;
    protected $fillable = ['meta', 'title', 'body', 'active', 'user_id'];
    /*********************************
     * Relación uno a muchos inversa *
     *********************************/
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    /*******************************************
     * Relación uno a uno polimórifica  *
     *******************************************/
    //este tipo de relación solo devuelve un único registro
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
    /*************************************
     * Relación uno a muchos polimórfica *
     *************************************/
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
    /****************************************
     * Relación muchos a muchos polimórfica *
     ****************************************/
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }
/***********************************************************************************************************************
 * Creamos un accesor para verificar la relación entre posts y tags y verificar si la cantidad de etiquetas asociasos al posts es mayo a la condición *
 ***********************************************************************************************************************/
    public function isActive(): Attribute
    {
        return Attribute::make(
            // verificamos que posts asociados a este usuario es mayor a cero
            get:fn() => $this->tags->count() > 0,
        );
    }

    /*************************
     * Trabajando con casts *
     *************************/
    protected $casts = [
        // 'meta' => 'array' //cast en el mismo modelo
        'meta' =>Meta::class,
        // PARA TRANSFORMAR LA FECHA
        'created_at' => 'date:Y-m-d'
    ];
    /*************************************************
     * OCULTAR CIERTO CAMPOS PARA QUE NO SE MUESTRAN *
     *************************************************/
    // protected $hidden = [
    //     'created_at',
    //     'updated_at'
    // ];
    /******************************************************
     * PARA MOSTRAR QUE CAMPOS QUEREMOS QUE SEAN VISIBLES *
     ******************************************************/
    // protected $visible = [
    //     'meta',
    //     'title',
    //     'body',
    //     'active',
    //     'user_id',
    //     'is_admin'
    // ];
/****************************************************
 * PARA AGREGAR A NUESTRA COLECCION NUESTRO MUTADOR *
 ****************************************************/
    // protected $appends = [
    //     'is_admin'
    // ];
    /*******************
     * OTROS MOTADORES *
     *******************/
    public function isAdmin():Attribute
    {
        return new Attribute(
            get: fn() => 'yes',
        );
    }
    /*****************************
     * PARA TRANSFORMAR LA FECHA *
     *****************************/

}
