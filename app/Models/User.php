<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Models\Course;
use App\Models\Post;
use App\Models\Role;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    /*****************************
     * TRABAJANDO CON RELACIONES *
     *****************************/
    //* RELACIÓN DE UNO A UNO
    public function phone()
    {
        return $this->hasOne(Phone::class)->withDefault(['user' => 'No tiene teléfono']);
    }
    //* RELACIÓN UNO A MUCHOS
    public function courses()
    {
        return $this->hasMany(Course::class);
    }
    public function posts()
    {
        return $this->hasMany(Post::class);
    }
    //* Relación para el último curso creado por el usuario
    public function latestCourses()
    {
        return $this->hasOne(Course::class)->latestOfMany();
    }
    //* Relación para curso mas antiguo creado por el usuario
    public function oldestCourses()
    {
        return $this->hasOne(Course::class)->oldestOfMany();
    }
    //* Relación muchos a muchos
    public function roles()
    {
        // as=> para cambiar el nombre de pivot por algo pesonalizado
        //withPivot => para indicar que campos mas quiero agregar
        // withTimestamps => para que se agrega la fecha de creado y actualizado
        //withPivot => para traer tambien el campo active
        return $this->belongsToMany(Role::class)->as('suscription')->withPivot('active')->withTimestamps();
    }

/***********************************************************************************************************************
 * Creamos un accesor para verificar la relación que existe entre usuarios y posts y verificar si la cantidad de libros asociasos al usuario es mayo a la condición *
 ***********************************************************************************************************************/
// Observación => no lo utilizamos en ninguna parte
    public function isActive(): Attribute
    {
        return Attribute::make(
            // verificamos que posts asociados a este usuario es mayor a cero
            get:fn() => $this->posts->count() > 0,
        );
    }

    /*******************************************************
     * Mutador para convertir los caracteres en minúsculas *
     *******************************************************/

    protected function name(): Attribute
    {
        /**************
        * OPCION #01 *
        **************/
        // return new Attribute(
        //     // Accesor => transforman el valor cuando estamos haciendo consulta a la base de datos o pedimos que nos retorno ciertos registros
        //     // Accesor => no transformar el valor en la base de datos solo la representación del registro retornado
        //     get:function($value){
        //        return ucwords($value);
        //     },
        //     // mutador => transforman el valor antes de almacenarlo
        //     set: function ($value) {
        //         return strtolower($value);
        //     }
        // );
    /**********************************************
     * OPCION #02 => UTILIZANDO LA FUNCIÓN FLECHA - RECOMENDADO*
     **********************************************/
        return new Attribute(
            //ACCESORES
            get:fn($value)=>ucwords($value),
            //MUTADORES
            set:fn($value)=>strtolower($value),
        );
    }
    // ANTIGUOS ACCESORES
    // public function getNameAttribute($value){
    //     return ucwords($value);
    // }
    // //ANTIGUOS MUTADORES
    // public function setNameAttribute($value){
    //     $this->attributes['name'] = strtolower($value);
    // }

}
