<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;
    protected $fillable = ['body'];
    /*************************************
     * Relación polimórfica uno a muchos *
     *************************************/
    public function commentable()
    {
        return $this->morphTo();
    }

}
