<?php

namespace App\Models;

//  Eliminación programada de modelos en Eloquent
// Para papelera de reciclaje de nuestros registros
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Prunable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Flight extends Model
{
    use HasFactory, SoftDeletes, Prunable;
    // protected $fillable = ['name', 'number', 'legs', 'active', 'departed', 'arrived_at', 'destination_id'];
    protected $guarded = ['id', 'created_at', 'update_at'];

    /*************************************************
     * Eliminación programada de modelos en Eloquent *
     *************************************************/
    public function prunable()
    {
        return static::where('departed', true);
    }
    /***************************************************
     * Para ejecutar antes que que comando model:prune *
     ***************************************************/
    // public function pruning()
    // {
    //     // Por ejemplo podemos eliminar todos los archivos asociados aun registro
    //     Storage::delete($this->image_url); ///es un ejemplo
    // }

    /*************************
     * Trabajando con scopes GLOBALES*
     *************************/
    // protected static function booted()
    // {
    //     //* Sin scope
    //     // static::addGlobalScope('not_departed', function (Builder $builder) {
    //     //     $builder->where('departed', false);
    //     // });
    //     //* con scope creado aparte
    //     static::addGlobalScope(new NotDepartedScope);
    // }
    /*********************************
     * TRABAJANDO CON SCOPES LOCALES *
     *********************************/
    public function scopeActive($query)
    {
        //* Para activos
        // $query->where('active', false);
        //* Para pasarle información al scope
        $query->where('active', false);
    }
    // para verificar los tramos que lo pasamos
    public function scopeLegs($query, $number)
    {
        $query->where('legs', $number);
    }
}
