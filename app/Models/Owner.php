<?php

namespace App\Models;

use App\Models\Car;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    use HasFactory;
    /******************************
     * Relación inversa uno a uno *
     ******************************/
    public function car()
    {
        return $this->belongsTo(Car::class);
    }
}
