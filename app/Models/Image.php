<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;
    /*************************
     * Relación polimórficas *
     *************************/
    //*Relación uno a uno polimórficas
    //el método igual al nombre del campo de la tabla
    public function imageable()
    {
        return $this->morphTo();
    }
}
