<?php

namespace App\Models;

use App\Models\Car;
use App\Models\Owner;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mechanic extends Model
{
    use HasFactory;
    /**********************
     * Relación uno a uno *
     **********************/
    public function car()
    {
        return $this->hasOne(Car::class);
    }

    //? RELACIÓN A TRAVÉS DE OTRO MODELO
    public function owner()
    {
        return $this->hasOneThrough(Owner::class, Car::class);
    }
}
