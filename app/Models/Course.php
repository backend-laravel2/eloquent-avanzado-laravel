<?php

namespace App\Models;

use App\Models\Comment;
use App\Models\Image;
use App\Models\Section;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;
    /*****************************
     * TRABAJANDO CON RELACIONES *
     *****************************/
    //* Relación uno a muchos inversa
    public function user()
    {
        return $this->belongsTo(User::class)->withDefault(['user' => 'No tiene Usuario']);
    }
    //* Relación uno a muchos
    public function sections()
    {
        // return $this->hasMany(Section::class,'course_id','id'); //cuando no sigues las convenciones de laravel tienes que pasarle mas parámetross
        return $this->hasMany(Section::class);
    }

    //? Relación a través de otro modelo
    public function lessons()
    {
        return $this->hasManyThrough(Lesson::class, Section::class);
    }
    /*******************************************
     * Relación uno a uno polimórifica  *
     *******************************************/
    //este tipo de relación solo devuelve un único registro
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
    /*************************************
     * Relación uno a muchos polimórfica *
     *************************************/
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
    //* Para traer el último comentario
    public function latestComment()
    {
        return $this->morphOne(Comment::class, 'commentable')->latestOfMany();
    }
    //* Para traer el comentario mas antiguo
    public function oldestComment()
    {
        return $this->morphOne(Comment::class, 'commentable')->oldestOfMany();
    }
    /****************************************
     * Relación muchos a muchos polimórfica *
     ****************************************/
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }
}
