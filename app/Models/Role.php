<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    /****************************
     * Relación muchos a muchos inversa?? *
     ****************************/
    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot('active');
    }
}
