<?php

namespace App\Models;

use App\Models\Lesson;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    use HasFactory;
    /*****************************
     * relación de unos a muchos *
     *****************************/
    public function lessons()
    {
        return $this->hasMany(Lesson::class);
    }
}
